package jp.co.techiemedia.pet.service;

import java.util.List;
import java.util.Optional;
import jp.co.techiemedia.pet.service.dto.PetsDTO;

/**
 * Service Interface for managing {@link jp.co.techiemedia.pet.domain.Pets}.
 */
public interface PetsService {
    /**
     * Save a pets.
     *
     * @param petsDTO the entity to save.
     * @return the persisted entity.
     */
    PetsDTO save(PetsDTO petsDTO);

    /**
     * Partially updates a pets.
     *
     * @param petsDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PetsDTO> partialUpdate(PetsDTO petsDTO);

    /**
     * Get all the pets.
     *
     * @return the list of entities.
     */
    List<PetsDTO> findAll();

    /**
     * Get the "id" pets.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PetsDTO> findOne(Long id);

    /**
     * Delete the "id" pets.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
