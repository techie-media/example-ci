package jp.co.techiemedia.pet.domain.enumeration;

/**
 * The PetType enumeration.
 */
public enum PetType {
    RACCOON,
    FOX,
    DOG,
    CAT,
}
