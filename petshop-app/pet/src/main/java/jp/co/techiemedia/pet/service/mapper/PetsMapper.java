package jp.co.techiemedia.pet.service.mapper;

import jp.co.techiemedia.pet.domain.Pets;
import jp.co.techiemedia.pet.service.dto.PetsDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pets} and its DTO {@link PetsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PetsMapper extends EntityMapper<PetsDTO, Pets> {}
