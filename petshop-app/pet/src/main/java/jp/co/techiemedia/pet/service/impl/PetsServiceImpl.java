package jp.co.techiemedia.pet.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import jp.co.techiemedia.pet.domain.Pets;
import jp.co.techiemedia.pet.repository.PetsRepository;
import jp.co.techiemedia.pet.service.PetsService;
import jp.co.techiemedia.pet.service.dto.PetsDTO;
import jp.co.techiemedia.pet.service.mapper.PetsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Pets}.
 */
@Service
@Transactional
public class PetsServiceImpl implements PetsService {

    private final Logger log = LoggerFactory.getLogger(PetsServiceImpl.class);

    private final PetsRepository petsRepository;

    private final PetsMapper petsMapper;

    public PetsServiceImpl(PetsRepository petsRepository, PetsMapper petsMapper) {
        this.petsRepository = petsRepository;
        this.petsMapper = petsMapper;
    }

    @Override
    public PetsDTO save(PetsDTO petsDTO) {
        log.debug("Request to save Pets : {}", petsDTO);
        Pets pets = petsMapper.toEntity(petsDTO);
        pets = petsRepository.save(pets);
        return petsMapper.toDto(pets);
    }

    @Override
    public Optional<PetsDTO> partialUpdate(PetsDTO petsDTO) {
        log.debug("Request to partially update Pets : {}", petsDTO);

        return petsRepository
            .findById(petsDTO.getId())
            .map(existingPets -> {
                petsMapper.partialUpdate(existingPets, petsDTO);

                return existingPets;
            })
            .map(petsRepository::save)
            .map(petsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PetsDTO> findAll() {
        log.debug("Request to get all Pets");
        return petsRepository.findAll().stream().map(petsMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PetsDTO> findOne(Long id) {
        log.debug("Request to get Pets : {}", id);
        return petsRepository.findById(id).map(petsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Pets : {}", id);
        petsRepository.deleteById(id);
    }
}
