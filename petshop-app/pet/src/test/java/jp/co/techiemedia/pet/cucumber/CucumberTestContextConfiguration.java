package jp.co.techiemedia.pet.cucumber;

import io.cucumber.spring.CucumberContextConfiguration;
import jp.co.techiemedia.pet.PetApp;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = PetApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
