package jp.co.techiemedia.pet;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import jp.co.techiemedia.pet.PetApp;
// import jp.co.techiemedia.pet.RedisTestContainerExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base composite annotation for integration tests.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(classes = PetApp.class)
// @ExtendWith(RedisTestContainerExtension.class)
public @interface IntegrationTest {
}
