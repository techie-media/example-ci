package jp.co.techiemedia.pet;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("jp.co.techiemedia.pet");

        noClasses()
            .that()
            .resideInAnyPackage("jp.co.techiemedia.pet.service..")
            .or()
            .resideInAnyPackage("jp.co.techiemedia.pet.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..jp.co.techiemedia.pet.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
