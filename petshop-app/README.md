# PetShop App

## Overview

![architecture.drawio.png](architecture.drawio.png)


## Requirement

```
❯ java --version
openjdk 11.0.12 2021-07-20 LTS
OpenJDK Runtime Environment Corretto-11.0.12.7.2 (build 11.0.12+7-LTS)
OpenJDK 64-Bit Server VM Corretto-11.0.12.7.2 (build 11.0.12+7-LTS, mixed mode)
---
❯ node --version
v14.18.1
---
❯ jhipster --version
INFO! Using JHipster version installed globally
7.3.1
```

## Initial setup

### Generate microservice application (pet)

```
❯ mkdir pet; cd pet;
❯ jhipster
? Which *type* of application would you like to create? Microservice application
? What is the base name of your application? pet
? Do you want to make it reactive with Spring WebFlux? No
? As you are running in a microservice architecture, on which port would like your server to run? It should be unique to
 avoid port conflicts. 8081
? What is your default Java package name? jp.co.techiemedia.pet
? Which service discovery server do you want to use? JHipster Registry (uses Eureka, provides Spring Cloud Config suppor
t and monitoring dashboards)
? Which *type* of authentication would you like to use? JWT authentication (stateless, with a token)
? Which *type* of database would you like to use? SQL (H2, PostgreSQL, MySQL, MariaDB, Oracle, MSSQL)
? Which *production* database would you like to use? MariaDB
? Which *development* database would you like to use? H2 with disk-based persistence
? Which cache do you want to use? (Spring cache abstraction) Redis (distributed cache)
? Do you want to use Hibernate 2nd level cache? Yes
? Would you like to use Maven or Gradle for building the backend? Maven
? Which other technologies would you like to use?
? Would you like to enable internationalization support? No
? Please choose the native language of the application English
? Besides JUnit and Jest, which testing frameworks would you like to use? (Press <space> to select, <a> to toggle all, ? Besides JUnit and Jest, which testing frameworks would you like to use?
? Would you like to install other generators from the JHipster Marketplace? No
```

### Generate entity (Pets)

```
❯ jhipster entity Pets
Generating field #1

? Do you want to add a field to your entity? Yes
? What is the name of your field? name
? What is the type of your field? String
? Do you want to add validation rules to your field? Yes
? Which validation rules do you want to add? Required, Minimum length, Maximum length
? What is the minimum length of your field? 1
? What is the maximum length of your field? 20

================= Pets =================
Fields
name (String) required minlength='1' maxlength='20'


Generating field #2

? Do you want to add a field to your entity? Yes
? What is the name of your field? type
? What is the type of your field? Enumeration (Java enum type)
? What is the class name of your enumeration? PetType
? What are the values of your enumeration (separated by comma, no spaces)? Raccoon,Fox,Dog,Cat
? Do you want to add validation rules to your field? Yes
? Which validation rules do you want to add? Required

================= Pets =================
Fields
name (String) required minlength='1' maxlength='20'
type (PetType) required


Generating field #3

? Do you want to add a field to your entity? Yes
? What is the name of your field? description
? What is the type of your field? String
? Do you want to add validation rules to your field? No

================= Pets =================
Fields
name (String) required minlength='1' maxlength='20'
type (PetType) required
description (String)


Generating field #4

? Do you want to add a field to your entity? Yes
? What is the name of your field? birthday
? What is the type of your field? LocalDate
? Do you want to add validation rules to your field? No

================= Pets =================
Fields
name (String) required minlength='1' maxlength='20'
type (PetType) required
description (String)
birthday (LocalDate)


Generating field #5

? Do you want to add a field to your entity? No

================= Pets =================
Fields
name (String) required minlength='1' maxlength='20'
type (PetType) required
description (String)
birthday (LocalDate)


Generating relationships to other entities

? Do you want to add a relationship to another entity? No

================= Pets =================
Fields
name (String) required minlength='1' maxlength='20'
type (PetType) required
description (String)
birthday (LocalDate)

? Do you want to use separate service class for your business logic? Yes, generate a separate service interface and i
mplementation
? Do you want to use a Data Transfer Object (DTO)? Yes, generate a DTO with MapStruct
? Do you want to add filtering? Not needed
? Is this entity read-only? No
? Do you want pagination and sorting on your entity? No
```

### Generate gateway (gateway)

```
❯ cd ../
❯ mkdir gateway; cd gateway;
❯ jhipster
? Which *type* of application would you like to create? Gateway application
? What is the base name of your application? gateway
? As you are running in a microservice architecture, on which port would like your server to run? It should be unique
 to avoid port conflicts. 8080
? What is your default Java package name? jp.co.techiemedia.gateway
? Which service discovery server do you want to use? JHipster Registry (uses Eureka, provides Spring Cloud Config sup
port and monitoring dashboards)
? Which *type* of authentication would you like to use? JWT authentication (stateless, with a token)
? Which *type* of database would you like to use? MongoDB
? Which cache do you want to use? (Spring cache abstraction) Redis (distributed cache)
? Would you like to use Maven or Gradle for building the backend? Maven
? Which other technologies would you like to use?
? Which *Framework* would you like to use for the client? Angular
? Do you want to generate the admin UI? Yes
? Would you like to use a Bootswatch theme (https://bootswatch.com/)? Default JHipster
? Would you like to enable internationalization support? No
? Please choose the native language of the application English
? Besides JUnit and Jest, which testing frameworks would you like to use?
? Would you like to install other generators from the JHipster Marketplace? No
```

### Generate entity (Pets)

```
❯ jhipster entity Pets
? Do you want to generate this entity from an existing microservice? Yes
? Enter the path to the microservice root directory: ../pet

Found the .jhipster/Pets.json configuration file, entity can be automatically generated!


The entity Pets is being updated.

? Do you want to update the entity? This will replace the existing files for this entity, all your custom code will b
e overwritten Yes, re generate the entity
```

### Generate docker-compose

```
❯ cd ../
❯ mkdir .docker-compose; cd .docker-compose;
❯ jhipster docker-compose
? Which *type* of application would you like to deploy? Microservice application
? Which *type* of gateway would you like to use? JHipster gateway based on Spring Cloud Gateway
? Enter the root directory where your gateway(s) and microservices are located ../
2 applications found at /Users/papa/Documents/git/jhipster/petshop-app/

? Which applications do you want to include in your configuration? gateway, pet
? Which applications do you want to use with clustered databases (only available with MongoDB and Couchbase)?
? Do you want to setup monitoring for your applications ? Yes, for metrics only with Prometheus
JHipster registry detected as the service discovery and configuration provider used by your apps
? Enter the admin password used to secure the JHipster Registry admin
...
To generate the missing Docker image(s), please run:
  ./mvnw -ntp -Pprod verify jib:dockerBuild in /path/petshop-app/gateway
  ./mvnw -ntp -Pprod verify jib:dockerBuild in /path/petshop-app/pet

You can launch all your infrastructure by running : docker-compose up -d
Congratulations, JHipster execution is complete!
...
```

### Build And Run
```
❯ cd ../pet
❯ ./mvnw -ntp -Pprod verify jib:dockerBuild
❯ cd ../gateway
❯ ./mvnw -ntp -Pprod verify jib:dockerBuild
❯ cd ../.docker-compose
❯ docker-compose up -d
```

