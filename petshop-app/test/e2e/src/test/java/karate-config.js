function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  if (!env) {
    env = 'dev';
  }

  baseUrl = karate.properties['karate.baseUrl']
  karate.log('karate.baseUrl system property was:', baseUrl);
  if (!baseUrl) {
    baseUrl = 'http://localhost:8080';
  }

  var config = {
    env: env,
    baseUrl: baseUrl
  }
  
  return config;
}