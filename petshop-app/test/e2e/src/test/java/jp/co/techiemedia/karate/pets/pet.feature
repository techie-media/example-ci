Feature: Pets feature e2e test

Background:
  * url baseUrl
  * def admin = 'admin'
  * def authenticate =
  """
  { "username": "#(admin)", "password": "#(admin)", "rememberMe": false}
  """
  Given path '/api/authenticate'
  And request authenticate
  When method post
  Then status 200
  * def token = response.id_token

  * def now = function(){ return java.lang.System.currentTimeMillis() }
  * def pet = 'test' + now()
  * def petBody =
  """
  {
    "name": "#(pet)",
    "type": "RACCOON",
    "description": "Description",
    "birthday": "2021-12-16"
  }
  """
Scenario: get pets
  Given path '/services/pet/api/pets'
  And header Authorization = 'Bearer ' + token
  When method get
  Then status 200

Scenario: create pet
  * def description = 'Description'
  Given path '/services/pet/api/pets'
  And request petBody
  And header Authorization = 'Bearer ' + token
  When method post
  Then status 201
  * def id = response.id

  Given path '/services/pet/api/pets/' + id
  And header Authorization = 'Bearer ' + token
  When method get
  Then status 200
  And assert response.name == pet
  And assert response.description == description

  Given path '/services/pet/api/pets/' + id
  And header Authorization = 'Bearer ' + token
  When method delete
  Then status 204

Scenario: edit pet
  * def description = 'Description-edit'
  Given path '/services/pet/api/pets'
  And request petBody
  And header Authorization = 'Bearer ' + token
  When method post
  Then status 201
  * def id = response.id

  Given path '/services/pet/api/pets/' + id
  And header Authorization = 'Bearer ' + token
  When method get
  Then status 200
  * def body = response
  * set body
  | path        | value       |
  | description | description |

  Given path '/services/pet/api/pets/' + id
  And request body
  And header Authorization = 'Bearer ' + token
  When method put
  Then status 200

  Given path '/services/pet/api/pets/' + id
  And header Authorization = 'Bearer ' + token
  When method get
  Then status 200
  And assert response.name == pet
  And assert response.description == description

  Given path '/services/pet/api/pets/' + id
  And header Authorization = 'Bearer ' + token
  When method delete
  Then status 204

Scenario: delete pet
  Given path '/services/pet/api/pets'
  And request petBody
  And header Authorization = 'Bearer ' + token
  When method post
  Then status 201
  * def id = response.id

  Given path '/services/pet/api/pets/' + id
  And header Authorization = 'Bearer ' + token
  When method get
  Then status 200

  Given path '/services/pet/api/pets/' + id
  And header Authorization = 'Bearer ' + token
  When method delete
  Then status 204

  Given path '/services/pet/api/pets/' + id
  And header Authorization = 'Bearer ' + token
  When method get
  Then status 404