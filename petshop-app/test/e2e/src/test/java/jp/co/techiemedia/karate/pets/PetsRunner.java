package jp.co.techiemedia.karate.pets;

import com.intuit.karate.junit5.Karate;

class PetsRunner {

   @Karate.Test
    Karate testPets() {
        return Karate.run("pets").relativeTo(getClass());
    }

}
