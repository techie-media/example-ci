Feature: Users feature e2e test

  Background:
    * url baseUrl
    * def admin = 'admin'
    * def authenticate =
    """
    { "username": "#(admin)", "password": "#(admin)", "rememberMe": false}
    """
    Given path '/api/authenticate'
    And request authenticate
    When method post
    Then status 200
    * def token = response.id_token

    * def now = function(){ return java.lang.System.currentTimeMillis() }
    * def user = 'test' + now()
    * def email = user + '@example.com'
    * def userBody =
    """
    {
      "login": "#(user)",
      "firstName": "test",
      "lastName": "test",
      "email": "#(email)",
      "activated": true,
      "langKey": "en",
      "authorities": ["ROLE_USER"]
    }
    """
  Scenario: get users
    Given path '/api/admin/users'
    And param sort = 'id,asc'
    And header Authorization = 'Bearer ' + token
    When method get
    Then status 200
    And assert response[0].login == admin

  Scenario: create user
    * def firstName = 'test'
    Given path '/api/admin/users'
    And request userBody
    And header Authorization = 'Bearer ' + token
    When method post
    Then status 201

    Given path '/api/admin/users/' + user
    And header Authorization = 'Bearer ' + token
    When method get
    Then status 200
    And assert response.login == user
    And assert response.firstName == firstName

    Given path '/api/admin/users/' + user
    And header Authorization = 'Bearer ' + token
    When method delete
    Then status 204

  Scenario: edit user
    * def firstName = 'test-edit'
    Given path '/api/admin/users'
    And request userBody
    And header Authorization = 'Bearer ' + token
    When method post
    Then status 201

    Given path '/api/admin/users/' + user
    And header Authorization = 'Bearer ' + token
    When method get
    Then status 200
    * def body = response
    * set body
    | path      | value     |
    | firstName | firstName |

    Given path '/api/admin/users'
    And request body
    And header Authorization = 'Bearer ' + token
    When method put
    Then status 200

    Given path '/api/admin/users/' + user
    And header Authorization = 'Bearer ' + token
    When method get
    Then status 200
    And assert response.login == user
    And assert response.firstName == firstName

    Given path '/api/admin/users/' + user
    And header Authorization = 'Bearer ' + token
    When method delete
    Then status 204

  Scenario: delete user
    Given path '/api/admin/users'
    And request userBody
    And header Authorization = 'Bearer ' + token
    When method post
    Then status 201

    Given path '/api/admin/users/' + user
    And header Authorization = 'Bearer ' + token
    When method get
    Then status 200
    And assert response.login == user

    Given path '/api/admin/users/' + user
    And header Authorization = 'Bearer ' + token
    When method delete
    Then status 204

    Given path '/api/admin/users/' + user
    And header Authorization = 'Bearer ' + token
    When method get
    Then status 404