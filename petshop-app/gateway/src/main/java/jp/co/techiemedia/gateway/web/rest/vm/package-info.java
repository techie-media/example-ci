/**
 * View Models used by Spring MVC REST controllers.
 */
package jp.co.techiemedia.gateway.web.rest.vm;
