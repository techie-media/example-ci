export enum PetType {
  RACCOON = 'RACCOON',

  FOX = 'FOX',

  DOG = 'DOG',

  CAT = 'CAT',
}
