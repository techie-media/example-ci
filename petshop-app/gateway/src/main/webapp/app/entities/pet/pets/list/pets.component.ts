import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPets } from '../pets.model';
import { PetsService } from '../service/pets.service';
import { PetsDeleteDialogComponent } from '../delete/pets-delete-dialog.component';

@Component({
  selector: 'jhi-pets',
  templateUrl: './pets.component.html',
})
export class PetsComponent implements OnInit {
  pets?: IPets[];
  isLoading = false;

  constructor(protected petsService: PetsService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.petsService.query().subscribe(
      (res: HttpResponse<IPets[]>) => {
        this.isLoading = false;
        this.pets = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IPets): number {
    return item.id!;
  }

  delete(pets: IPets): void {
    const modalRef = this.modalService.open(PetsDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.pets = pets;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
