jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { PetsService } from '../service/pets.service';
import { IPets, Pets } from '../pets.model';

import { PetsUpdateComponent } from './pets-update.component';

describe('Pets Management Update Component', () => {
  let comp: PetsUpdateComponent;
  let fixture: ComponentFixture<PetsUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let petsService: PetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PetsUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(PetsUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PetsUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    petsService = TestBed.inject(PetsService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const pets: IPets = { id: 456 };

      activatedRoute.data = of({ pets });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(pets));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Pets>>();
      const pets = { id: 123 };
      jest.spyOn(petsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ pets });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: pets }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(petsService.update).toHaveBeenCalledWith(pets);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Pets>>();
      const pets = new Pets();
      jest.spyOn(petsService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ pets });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: pets }));
      saveSubject.complete();

      // THEN
      expect(petsService.create).toHaveBeenCalledWith(pets);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Pets>>();
      const pets = { id: 123 };
      jest.spyOn(petsService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ pets });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(petsService.update).toHaveBeenCalledWith(pets);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
