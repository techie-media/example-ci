import * as dayjs from 'dayjs';
import { PetType } from 'app/entities/enumerations/pet-type.model';

export interface IPets {
  id?: number;
  name?: string;
  type?: PetType;
  description?: string | null;
  birthday?: dayjs.Dayjs | null;
}

export class Pets implements IPets {
  constructor(
    public id?: number,
    public name?: string,
    public type?: PetType,
    public description?: string | null,
    public birthday?: dayjs.Dayjs | null
  ) {}
}

export function getPetsIdentifier(pets: IPets): number | undefined {
  return pets.id;
}
