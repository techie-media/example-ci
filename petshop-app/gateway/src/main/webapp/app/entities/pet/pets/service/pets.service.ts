import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPets, getPetsIdentifier } from '../pets.model';

export type EntityResponseType = HttpResponse<IPets>;
export type EntityArrayResponseType = HttpResponse<IPets[]>;

@Injectable({ providedIn: 'root' })
export class PetsService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/pets', 'pet');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(pets: IPets): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(pets);
    return this.http
      .post<IPets>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(pets: IPets): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(pets);
    return this.http
      .put<IPets>(`${this.resourceUrl}/${getPetsIdentifier(pets) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(pets: IPets): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(pets);
    return this.http
      .patch<IPets>(`${this.resourceUrl}/${getPetsIdentifier(pets) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPets>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPets[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPetsToCollectionIfMissing(petsCollection: IPets[], ...petsToCheck: (IPets | null | undefined)[]): IPets[] {
    const pets: IPets[] = petsToCheck.filter(isPresent);
    if (pets.length > 0) {
      const petsCollectionIdentifiers = petsCollection.map(petsItem => getPetsIdentifier(petsItem)!);
      const petsToAdd = pets.filter(petsItem => {
        const petsIdentifier = getPetsIdentifier(petsItem);
        if (petsIdentifier == null || petsCollectionIdentifiers.includes(petsIdentifier)) {
          return false;
        }
        petsCollectionIdentifiers.push(petsIdentifier);
        return true;
      });
      return [...petsToAdd, ...petsCollection];
    }
    return petsCollection;
  }

  protected convertDateFromClient(pets: IPets): IPets {
    return Object.assign({}, pets, {
      birthday: pets.birthday?.isValid() ? pets.birthday.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.birthday = res.body.birthday ? dayjs(res.body.birthday) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((pets: IPets) => {
        pets.birthday = pets.birthday ? dayjs(pets.birthday) : undefined;
      });
    }
    return res;
  }
}
