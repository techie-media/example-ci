import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { PetsService } from '../service/pets.service';

import { PetsComponent } from './pets.component';

describe('Pets Management Component', () => {
  let comp: PetsComponent;
  let fixture: ComponentFixture<PetsComponent>;
  let service: PetsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PetsComponent],
    })
      .overrideTemplate(PetsComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PetsComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PetsService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.pets?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
