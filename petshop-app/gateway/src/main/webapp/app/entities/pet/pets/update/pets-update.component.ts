import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IPets, Pets } from '../pets.model';
import { PetsService } from '../service/pets.service';
import { PetType } from 'app/entities/enumerations/pet-type.model';

@Component({
  selector: 'jhi-pets-update',
  templateUrl: './pets-update.component.html',
})
export class PetsUpdateComponent implements OnInit {
  isSaving = false;
  petTypeValues = Object.keys(PetType);

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.minLength(1), Validators.maxLength(20)]],
    type: [null, [Validators.required]],
    description: [],
    birthday: [],
  });

  constructor(protected petsService: PetsService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ pets }) => {
      this.updateForm(pets);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const pets = this.createFromForm();
    if (pets.id !== undefined) {
      this.subscribeToSaveResponse(this.petsService.update(pets));
    } else {
      this.subscribeToSaveResponse(this.petsService.create(pets));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPets>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(pets: IPets): void {
    this.editForm.patchValue({
      id: pets.id,
      name: pets.name,
      type: pets.type,
      description: pets.description,
      birthday: pets.birthday,
    });
  }

  protected createFromForm(): IPets {
    return {
      ...new Pets(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      type: this.editForm.get(['type'])!.value,
      description: this.editForm.get(['description'])!.value,
      birthday: this.editForm.get(['birthday'])!.value,
    };
  }
}
