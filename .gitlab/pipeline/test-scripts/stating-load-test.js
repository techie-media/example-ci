import http from 'k6/http';
import { check, sleep } from 'k6';

export const options = {
  hosts: { 'stg-petshop.example.com': '<Ingress IP Address>' },
  vus: 10,
  duration: '15s',
};

export default function () {
  const res = http.get('http://stg-petshop.example.com/');
  check(res, { 'status was 200': (r) => r.status == 200 });
  sleep(1);
}
